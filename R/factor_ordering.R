factor_ordering<-function(
  data,
  factor_order=NULL,
  uniqueLines=NULL,
  uniqueColumns=NULL,
  uniqueScatters=NULL,
  aesthetic_variable=NULL
){

  if(!is.null(uniqueLines)){
    if(!(uniqueLines%in%factor_order)){
      factor_order<-c(uniqueLines,factor_order)
    }
  }

  if(!is.null(uniqueScatters)){
    if(!(uniqueScatters%in%factor_order)){
      factor_order<-c(uniqueScatters,factor_order)
    }
  }

  data<-dplyr::mutate(data, {{aesthetic_variable}} := factor({{aesthetic_variable}}, levels= factor_order))

  return(data)
}
