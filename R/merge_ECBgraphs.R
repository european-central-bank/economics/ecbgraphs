#' Merging two ECB-graphs



#' @param left_chart  A plot made using either \itemize{
#'   \item ECBgraphs
#'   \item ggplot2 with \code{ECB_theme}
#' }

#' @param right_chart  A plot made using either \itemize{
#'   \item ECBgraphs
#'   \item ggplot2 with \code{ECB_theme}
#' }

#' @param sameLegend  A boolean that if \code{TRUE} merges the legend from
#' \code{left_chart} with the legend from \code{right_chart}. The
#' merged legend will be placed on top of the graph.


#' @param sameAxis  A boolean that if \code{TRUE} merges the axis from
#' \code{left_chart} with the axis from \code{right_chart}. The
#' axis will be scaled so that the maxiumn and minumum values for both charts fit on
#' the same axis. Note that this would over-write the previous axis values. As such,
#' should specific axis values be desired, please adjust the individual charts and
#' set  \code{sameAxis = FALSE}.

#' @author European Central Bank




merge_ECBgraphs<-function(
  left_chart,
  right_chart,
  sameLegend=TRUE,
  sameAxis=FALSE,
  style="outputWindow",
  dimensions=c(1,1),
  left_panelTitle=NULL,
  right_panelTitle=NULL,
  left_rotate_xLabels=FALSE,
  right_rotate_xLabels=FALSE
){

  # http://www.sthda.com/english/articles/32-r-graphics-essentials/126-combine-multiple-ggplots-in-one-graph/

    if (sameAxis==TRUE){

    labRange<-rbind(
      ggplot2::layer_scales(left_chart)$y$range$range,
      ggplot2::layer_scales(right_chart)$y$range$range
    )

    lBound<-min(labRange[,1])
    uBound<-max(labRange[,2])

    left_chart<-left_chart+
          ggplot2::scale_y_continuous(
            limits = c(lBound,uBound),
            breaks = scales::pretty_breaks(n=6)
          )
    right_chart<-right_chart+
      ggplot2::scale_y_continuous(
        limits = c(lBound,uBound),
        breaks = scales::pretty_breaks(n=6)
      )


  }



  LP<-left_chart+adjustTheme(
    outputFormat=style,
    merge_margin=TRUE,
    rotate_xLabels=left_rotate_xLabels
  )+ggplot2::labs(title=left_panelTitle)



  RP<-right_chart+adjustTheme(
      outputFormat=style,
      merge_margin=TRUE,
      merge_w_sameAxis=sameAxis,
      rotate_xLabels=right_rotate_xLabels
  )+ggplot2::labs(title=right_panelTitle)




  ggpubr::ggarrange(
    LP,
    RP,
    common.legend = sameLegend,
    legend = "top",
    widths = dimensions
  )

}
