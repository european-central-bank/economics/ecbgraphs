
ribbon_modification<-function (d, var, x, y, ribbon, panel_var)
{
  dR <- dplyr::filter(d, !!as.name(rlang::get_expr(var)) %in%  unlist(ribbon))

  if (!rlang::quo_is_null(panel_var)) {
    dR <- dplyr::group_by(dR, !!x)
    dR <- dplyr::group_by(dR, !!panel_var, .add = T)
  }
  else {
    dR <- dplyr::group_by(dR, !!x)
  }


  if(is.list(ribbon)){

    ribbon_geoms <-
      lapply(ribbon, function(g){
        dR <- dplyr::filter(dR, !!as.name(rlang::get_expr(var)) %in% g)
        dR <- dplyr::summarise(dR,
                               lowerBound = min(!!y, na.rm=TRUE),
                               upperBound = max(!!y, na.rm = TRUE))

        ggplot2::geom_ribbon(data = dR,
                             ggplot2::aes(x = !!x,
                                          ymin = lowerBound,
                                          ymax = upperBound),
                             alpha = 0.25)
      })

    return(ggplot2::ggplot(data = dR)  + ribbon_geoms)
  }

  dR <- dplyr::summarise(dR, lowerBound = min(!!y, na.rm = TRUE),
                         upperBound = max(!!y, na.rm = TRUE))
  p <- ggplot2::ggplot(data = dR) +
    ggplot2::geom_ribbon(data = dR,
                         ggplot2::aes(x = !!x,
                                      ymin = lowerBound,
                                      ymax = upperBound),
                         alpha = 0.25,
                         col = "grey")
}
