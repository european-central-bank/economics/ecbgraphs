#' Make a combo chart consistent with ECB formatting guidelines

#' @description
#' ECB_comboChart is a function developed to permit flexible chart layouts to visualise economic time series data.
#' The function is particularly useful for, but not limited to, time series graphs.
#' However, it is highly encourage only to use the ECB_comboChart in cases where; \itemize{
#' \item \code{ECB_lineChart},
#' \item \code{ECB_decompositionChart},
#' }
#' don't suffice. The reason being that those are more extensively tested and require less input-values.


#' @param data A data set (preferably long format and retrieved  from internal data sources).

#' @param x The column name of the variable for the x-axis (as string).
#' By default \code{x = "OBS_DATE"}.

#' @param y The column name of the variable for the y-axis (as string).
#' By default \code{y = "OBS_VALUE"}.

#' @param aesthetic_variable The column name of the categorical variable (as string) intended for the aesthetics of the lines.
#' Note that the unique values in the \code{aesthetic_variable} will be displayed in the legend.
#' By default \code{aesthetic_variable = "SERIES_NAME"}.
#' In case a single-line chart is desired, set \code{aesthetic_variable = NULL}.

#' @param LaneLine A boolean that if \code{TRUE} add the last observation to the graph. If  \code{LaneLine=TRUE},
#' then the last observation is highlighted by (i) a vertical line in same format as the minor gridlines,
#' and (ii) included as label on the horizontal axis.
#' Depending on the frequency of the data, date labels will be rendered as follows;\itemize{
#' \item yearly data as 2020,
#' \item quarterly data as 2020Q1 or 20Q1,
#' \item monthly data as Jan-2020 or Jan-20,
#' \item daily/weekly data as 01-Jan-2020.
#'
#' }

#' @param lhs_line A list of series which will be displayed as lines against the left hand side axis
#' @param lhs_column A list of series which will be displayed as columns against the left hand side axis
#' @param lhs_point A list of series which will be displayed as points against the left hand side axis

#' @param rhs_line A list of series which will be displayed as lines against the right hand side axis
#' @param rhs_point A list of series which will be displayed as points against the right hand side axis

#' @param panel_var The column name of the variable (as string)
#' intended to faceting groups on the rows or columns dimension.
#' Put differently, including a \code{panel_var} yields multi-panel plots.

#' @param manual_panel_scale String to control the scales of the panels.
#' Could be any of the following: \itemize{
#' \item "fixed" - (default) same y- and x-axis for each panel
#' \item "free" - individual y- and x-axis for each panel
#' \item free in one dimension: "free_x" or"free_y"
#' }

#' @param y_axis_same_origin A boolean that if \code{TRUE} puts the left y-axis and the right y-axis on same origin.
#' Particularly useful when the left axis displays a percentage and the right axis displays a level.
#' For example, in case the left axis reflects real GDP and the right axis reflects PMI, then setting
#' \code{y_axis_same_origin=TRUE} transforms the axes so that 0 on the left axis is displayed at same tick as
#' 50  on right axis.
#'
#' @param y_axis_rhs_invert A boolean that if \code{TRUE} inverts
#' (i) all series in \code{rhs_series} and (ii) the right axis.

#' @param y_axis_duplicate A boolean that if \code{TRUE} duplicates the left y-axis to the right side.
#' By default, \code{y_axis_duplicate=FALSE}

#' @param manual_legend_rhs_text  A string that will be pasted in the legend after all series on the right axis.
#' By default, " (rhs)" is pasted after each series for the right axis.

#' @param manual_y_axis_rhs_min A numeric value to control the scale of the right y-axis.
#' In short, this is a substitute for manually controlling the entire right axis values.
#' Such adjustment is not be supported as the right axis is a transformation of the left axis.

#' @param manual_y_axis_rhs_max A numeric value to control the scale for the right y-axis.
#' In short, this is a substitute for manually controlling the entire right axis values.
#' Such adjustment is not be supported as the right axis is a transformation of the left axis.

#' @param format_x_title  Title of the x-axis (as string).
#' Note that this should also be added in the \code{save_ECBgraph}.

#' @param format_y_title  Title of the y-axis (as string).
#'  Note that this should also be added in the \code{save_ECBgraph}.

#' @param format_rotate_labels A boolean that if \code{TRUE} rotates the labels on the horizontal axis 90 degrees.
#'  Note that this should also be added in the \code{save_ECBgraph}.

#' @param keep_default_ggplot2_horizontalAxis  A boolean that if \code{TRUE} ignores the predetermined formatting
#'  for the horizontal axis.

#' @param add_horizontal_line A numeric value(s) intended to be displayed as a horizontal line(s).
#' Particularly useful to plot PMI's and set \code{add_horizontal_line=50}
#' or y-o-y percentage changes and set \code{add_horizontal_line=0}.

#' @param add_vertical_line A value(s) of same format as the x-axis
#' representing the value(s) where vertical line(s) should be added.
#' Particularly useful to indicate forecast period, e.g. \code{add_vertical_line=as.Date("2050-01-01")}.

#' @param manual_y_axis_lhs  A numeric vector to control the values of the left y-axis.
#' Note that the limits of the y-axis are set to the max and min value of \code{manual_y_axis_lhs}, which might
#' (unintentionally) remove outliers from the graph. As such, \code{manual_y_axis_lhs=NULL} is advised for all
#' automated graphs.


#' @param manual_y_axis_lhs_decimals A numeric value to control the number of decimals on the left y-axis.

#' @param manual_y_axis_rhs_decimals A numeric value to control the number of decimals on the right y-axis.

#' @param manual_linetypes  A named list of all unique values in the \code{aesthetic_variable} and the respective line type.
#' The name of the entry should be the series to which the linetype should be applied
#' The line type can be any of the following: \itemize{
#' \item "solid" (default)
#' \item "twodash"
#' \item "longdash"
#' \item "dotted"
#' \item "dotdash"
#' \item "dashed"
#' \item "blank"
#' }
#'
#' @param range_series A vector of the series (as strings) intended to be displayed as min-max range in the chart.
#' If multiple ranges are desired you can use a list containing vectors of the series intended to be contained in each range.
#' Note that none of the values in \code{range_series} are displayed in the chart.
#' Note also that the \code{range_series} must correspond to values in the \code{aesthetic_variable}.







#' @author European Central Bank

#' @export
ECB_comboChart<-function(
  data,
  x = "OBS_DATE",
  y = "OBS_VALUE",
  aesthetic_variable = "SERIES_NAME",
  LaneLine = TRUE,
  lhs_line = NULL,
  lhs_column = NULL,
  lhs_point = NULL,
  rhs_line = NULL,
  rhs_point = NULL,
  panel_var = NULL,
  manual_panel_scale = "fixed",
  y_axis_same_origin = FALSE,
  y_axis_rhs_invert = FALSE,
  y_axis_duplicate = FALSE,
  manual_y_axis_lhs = NULL,
  manual_y_axis_rhs_min = NULL,
  manual_y_axis_rhs_max = NULL,
  manual_y_axis_lhs_decimals  = NULL,
  manual_y_axis_rhs_decimals = NULL,
  manual_legend_rhs_text  = "(rhs)",
  manual_legend_order = NULL,
  manual_colours = NULL,
  manual_linetypes = NULL,
  add_vertical_line = NULL,
  add_horizontal_line = NULL,
  format_x_title = NULL,
  format_y_title = NULL,
  format_rotate_labels = FALSE,
  keep_default_ggplot2_horizontalAxis = FALSE,
  y_axis_symmetric_origin = FALSE,
  range_series = NULL
){

  d <- data
  x <- rlang::ensym(x)
  y <- rlang::ensym(y)
  aesthetic_variable <- rlang::enquo(aesthetic_variable)
  if(!rlang::quo_is_symbol(aesthetic_variable)){
    aesthetic_variable_string <- rlang::get_expr(aesthetic_variable)
    aesthetic_variable <- rlang::sym(aesthetic_variable_string)
    aesthetic_variable_quo <- rlang::enquo(aesthetic_variable)
  } else{
    aesthetic_variable_string <- rlang::as_string(rlang::get_expr(aesthetic_variable))
    aesthetic_variable_quo <- aesthetic_variable
  }


  rhs_vars<-c(rhs_line,rhs_point)
  lhs_vars<-c(lhs_line,lhs_column,lhs_point)

  line_series<-c(rhs_line,lhs_line)
  column_series<-c(lhs_column)
  point_series<-c(rhs_point,lhs_point)

  combochart_stopwarn(line_series,
                      column_series,
                      point_series,
                      rhs_vars,
                      lhs_vars)


    lineData<-dplyr::filter(d,!!aesthetic_variable %in% intersect(line_series, lhs_vars))
    pointData<-dplyr::filter(d,!!aesthetic_variable %in% intersect(point_series, lhs_vars))
    columnData<-dplyr::filter(d, !!aesthetic_variable %in% intersect(column_series, lhs_vars))

    if(!is.null(range_series)){
      panel_variable <- rlang::enquo(panel_var)
      p <- ribbon_modification(
        d=d,
        var={{aesthetic_variable}},
        x= {{x}},
        y= {{y}},
        ribbon=range_series,
        panel_var=panel_variable
      )

      d <-dplyr::filter(d, !(.data[[aesthetic_variable]] %in% unlist(range_series)))

    } else{

    p<-ggplot2::ggplot(data=d)#+ggplot2::aes()
    }

    # Left hand side variables
    if(!is.null(column_series)){
      p<-p + ggplot2::geom_col(
        data = columnData,
        ggplot2::aes(x = !!x,
                     y = !!y,
                     fill = !!aesthetic_variable )
      )

    }

    if(!is.null(line_series)){
      p <- p +
        ggplot2::geom_line(
          data = lineData,
          ggplot2::aes(x = !!x,
                       y = !!y,
                       col = !!aesthetic_variable), size = 1.05)
    }

    if(!is.null(point_series)){
      p <- p +  ggplot2::geom_point(
          data = pointData,
          ggplot2::aes(x = !!x,
                       y = !!y,
                       col = !!aesthetic_variable),
          size  = 2)

    }




  #/// Want to include adding a second series here
  if(!is.null(rhs_vars)){
    aesthetic_variable_original_string <- paste0(aesthetic_variable_string , "_ORIGINAL")
    aesthetic_variable_original <- rlang::sym(aesthetic_variable_original_string)
    aesthetic_variable_original_quo <- rlang::enquo(aesthetic_variable_original)
    y_original_string <- paste0(rlang::get_expr(y), "_ORIGINAL")
    y_original <- rlang::sym(y_original_string)

    dRHS <-  dplyr::filter(data,{{aesthetic_variable}} %in% rhs_vars )
    dRHS <-  dplyr::mutate(dRHS,
                           {{aesthetic_variable_original_quo}} := {{aesthetic_variable}},
                           {{y_original}} := {{y}})





    p<- p + secondary_axis_p(
      p,
      dRHS,
      aesthetic_variable = {{aesthetic_variable}},
      same_origo = y_axis_same_origin,
      lhs_line = lhs_line,
      lhs_column = lhs_column,
      lhs_point = lhs_point,
      rhs_line = rhs_line,
      rhs_point = rhs_point,
      indicate_rhs_by = manual_legend_rhs_text,
      invert_rhs = y_axis_rhs_invert,
      rhs_min = manual_y_axis_rhs_min,
      rhs_max = manual_y_axis_rhs_max,
      manual_lhs_axis = manual_y_axis_lhs,
      decimals_lhs = manual_y_axis_lhs_decimals,
      decimals_rhs = manual_y_axis_rhs_decimals,
      symmetric_origo = y_axis_symmetric_origin,
      x = {{x}} ,
      y = {{y}},
    )

  }
  #///

  # Charttype is primarily relevant for decompositions chart,
  # i.e. those with column series.
  # Specifically, they must have different attributes for
  # horizontal axis (limits) vertical axis (expand)
  if(is.null(column_series) ){
    adj_legend_width<-1
    chartType<-"lineChart"
  }else {
    adj_legend_width<-0.4
    chartType<-"decompositionChart"

  }

  if(is.null(rhs_vars)){
    min_val <- min(dplyr::pull(dplyr::select(d, !!y)), na.rm = T)
    max_val <- max(dplyr::pull(dplyr::select(d, !!y)), na.rm = T)
    p<-vertical_axis(
      p,
      chartType = chartType,
      manual_change = manual_y_axis_lhs,
      minValue = min_val,
      maxValue = max_val,
      decimals_lhs = manual_y_axis_lhs_decimals,
      duplicate_y = y_axis_duplicate
    )

  }


  p<-p+adjustTheme(
    xLab = format_x_title,
    yLab = format_y_title,
    rotate_xLabels = format_rotate_labels,
    legend_width = adj_legend_width
  )+ggplot2::xlab(paste0("\n",format_x_title))+
    ggplot2::ylab(paste0(format_y_title,"\n"))


  if(!is.null(point_series) &
     !is.null(line_series) &
     is.null(column_series) &
     !(all(line_series == point_series))){
    line_point_special<-TRUE
  } else {
    line_point_special<-FALSE

    # This one should move to colour_modification later
    # p<-legend_modification(
    #   p,
    #   chartType = chartType,
    #   uniqueLines = line_series,
    #   uniqueColumns = column_series,
    #   uniqueScatters = NULL,
    #   legend_nrow = manual_legend_nrow
    # )


  }

  if(y_axis_rhs_invert & manual_legend_rhs_text == '(rhs)' ){
    manual_legend_rhs_text <- "(rhs, inverted)"
  }

  manual_colours <- update_item_names(manual_colours,
                                      'manual_colours',
                                      rhs_vars,
                                      manual_legend_rhs_text)

  manual_linetypes <- update_item_names(manual_linetypes,
                                        'manual_linetypes',
                                        rhs_vars,
                                        manual_legend_rhs_text)


  updater <- update_man_leg_ord_col(manual_legend_order,
                                    rhs_vars,
                                    manual_legend_rhs_text,
                                    manual_colours,
                                    manual_linetypes)

  manual_legend_order <- updater[[1]]
  manual_colours <- updater[[2]]
  manual_linetypes <- updater[[3]]

    if(!is.null(rhs_line)){
      line_series <- c(paste(rhs_line, manual_legend_rhs_text), lhs_line)
    }
    if(!is.null(rhs_point)){
      point_series <- c(paste(rhs_point, manual_legend_rhs_text), lhs_point)
    }


  p<-colour_modification(
    p,
    line_series = line_series,
    column_series = column_series,
    scatter_series = point_series,
    change_col_manual = manual_colours,
    change_line_manual = manual_linetypes,
    legend_nrow = manual_legend_nrow,
    chartType = "comboChart",
    manual_legend_order = manual_legend_order
  )


  if(is.null(column_series) & is.null(point_series)){
    chartType<-"lineChart"
  }else {
    chartType<-"decompositionChart"
  }

  # line with points also requires additional space
  # -> expand  = c(0,0) will only work for lineChart
  p<-horizontal_x_inputValidation(
    p,
    chartType = chartType,
    d = d,
    x = x,
    add_LaneLine = LaneLine,
    ggplot2_Axis = keep_default_ggplot2_horizontalAxis,
    var = aesthetic_variable_string
  )


  if(!is.null(add_vertical_line)){
    p<-p+ggplot2::geom_vline(xintercept = add_vertical_line, col = dark_grey )
  }


  if(!is.null(add_horizontal_line)){
    p<-p+ggplot2::geom_hline(yintercept = add_horizontal_line, col = dark_grey)
  }



  if(!is.null(panel_var)){

    p<-facet_ECB(
      p,
      panel_var = panel_var,
      change_panel_scale = manual_panel_scale
    )

  }


  p




}

combochart_stopwarn<- function(line_series,
                               column_series,
                               point_series,
                               rhs_var,
                               lhs_var){
  all_series <- c(line_series, column_series, point_series)
  len_line_series <- length(line_series)
  len_column_series <- length(column_series)
  len_point_series <- length(point_series)
  len_all_series <- length(all_series)
  len_rhs_var <- length(rhs_var)
  if(len_all_series==0){
    stop(paste("ECB_comboChart requires you should specify at least two of",
               "`lhs_line`, `lhs_column`, `lhs_point`, `rhs_line`, and `rhs_point`"), call. = F)
  }
  if(len_line_series == 0 &
     len_point_series == 0 &
     len_rhs_var ==0) {
    warning(paste("ECB_combChart warning:",
                  "You appear to be using a simple column chart.",
                  "Try using ECB_columnChart for more stable results."), call. = F)
  }
  if(len_column_series == 0 &
     len_point_series == 0 ) {
    warning(paste("ECB_combChart warning:",
                  "You appear to be using a line chart.",
                  "Try using ECB_lineChart for more stable results."), call. = F)
  }
  if(len_column_series == 0 &
     len_line_series == 0 ) {
    warning(paste("ECB_combChart warning:",
                  "You appear to be using a scatter chart.",
                  "Try using ECB_scatterChart for more stable results."), call. = F)
  }
  if(len_line_series == 0 &
     len_point_series == 0 &
     len_rhs_var > 0) {
    warning(paste("ECB_combChart warning:",
                  "You appear to be using a column chart with two axis.",
                  "It is often better to use line series in this context."), call. = F)
  }
  if(len_line_series == 0 &
     len_column_series == 0 &
     len_rhs_var > 0) {
    warning(paste("ECB_combChart warning:",
                  "You appear to be using a scatter chart with two axis.",
                  "Please consider the readability of your chart."), call. = F)
  }

}
