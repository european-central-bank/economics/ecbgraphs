secondary_axis_ <- function(
  d,
  indicate_rhs_by="(rhs)",
  invert_rhs=FALSE,
  manual_lhs_axis=NULL,
  lhs_line=NULL,
  lhs_column=NULL,
  lhs_point=NULL,
  rhs_line=NULL,
  rhs_point=NULL,
  rhs_min = NULL,
  rhs_max = NULL,
  decimals_lhs=NULL,
  decimals_rhs = NULL,
  same_origo=FALSE,
  symmetric_origo = FALSE,
  aesthetic_variable = 'var'
){

  # LABELS ----
  if (invert_rhs == TRUE){
    rhs_labs <- function(x) {
      if(is.null(decimals_rhs)) {
        format((x*-1), scientific = F)
      }
      else {
        if(decimals_rhs == 0){
          rhs_frmt <- "%#"
        }
        else {
          rhs_frmt <- "%#."
        }
        format(sprintf((x*-1), fmt = paste0(rhs_frmt, decimals_rhs, "f")), scientific = F)
      }
    }
    if(indicate_rhs_by == "(rhs)"){
      indicate_rhs_by <- "(rhs, inverted)"
    }
  }
  else {
    rhs_labs <- function(x) {
      if(is.null(decimals_rhs)) {
        format((x), scientific = F)
      } else {
        if(decimals_rhs == 0){
          rhs_frmt <- "%1."
      } else {
        rhs_frmt <- "%#."
      }
      format(sprintf((x), fmt = paste0(rhs_frmt, decimals_rhs, "f")), scientific = F)}
    }
  }

  lhs_labs <- function(x) {
    if(is.null(decimals_lhs)) {
      format((x), scientific = F)
    }
    else {
      if(decimals_lhs == 0){
        lhs_frmt <- "%1."
      }
      else {
        lhs_frmt <- "%#."
      }
      format(sprintf((x), fmt = paste0(lhs_frmt, decimals_lhs, "f")), scientific = F)
      }
   }

  # Data  frame set-up -------

  rhs_var <- c(rhs_line, rhs_point)

  lhs_var <- c(lhs_line, lhs_column, lhs_point)

  line_series <- c(rhs_line, lhs_line)
  column_series <- c(lhs_column)
  point_series <- c(rhs_point, lhs_point)

  # This is a dirty trick to allow us to re-use existing
  # code without having to do much by way of tidy evaluation
  # should be refactored, in particular because the chart's
  # data becomes obscured using this method
  d <- dplyr::rename(d, var = aesthetic_variable)
  dLHS <- dplyr::filter(d, !(var %in% rhs_var))
  dRHS <- dplyr::filter(d, (var %in% rhs_var))
  if (invert_rhs == TRUE){
    dRHS$y <- -dRHS$y
  }
  dRHS$var <- paste(dRHS$var, indicate_rhs_by)

  # Max and Min of axes ------
  if(is.null(rhs_max)) r_max <- max(dRHS$y, na.rm = T) else r_max <- rhs_max
  if(is.null(rhs_min)) r_min <- min(dRHS$y, na.rm = T) else r_min <- rhs_min

  l_max <- max(dLHS$y, na.rm = T)
  l_min <- min(dLHS$y, na.rm = T)


  # -----
  # To retreive the max and minimun required for the graph
  # And make nice LHS axis
  if(is.null(manual_lhs_axis)){
    lhsP <- ggplot2::ggplot(data = dLHS, ggplot2::aes(x=x, y=y))
    lhs_Limits <- ggplot2::layer_scales(lhsP)$y$range$range
    draft_prettyLHS <- pretty(seq(lhs_Limits[1], lhs_Limits[2]))

  } else {
    draft_prettyLHS <- manual_lhs_axis
  }


  rhsP <- ggplot2::ggplot(data = dRHS, ggplot2::aes(x=x, y=y))
  rhs_Limits <- ggplot2::layer_scales(rhsP)$y$range$range
  prettyRHS <- pretty(seq(rhs_Limits[1], rhs_Limits[2]))
  if(max(prettyRHS) == 0) {
    # there is a risk that we get a division by 0 error
    # the next line takes care of this
    prettyRHS <- pretty(seq(rhs_Limits[1], 1))
  }

  if (same_origo == TRUE){
    potentialOrigo <- c(0, 50, 100)

    oLHS <- potentialOrigo[which.min(abs(potentialOrigo - mean(prettyLHS)))]
    oRHS <- potentialOrigo[which.min(abs(potentialOrigo - mean(prettyRHS)))]

    # OldRange denotes the desired range above the x-axis
    OldRange = max(prettyRHS) - oRHS
    NewRange = max(prettyLHS) - oLHS


    t_min  <- ((r_min-oRHS)*NewRange/OldRange)
    t_max  <- ((r_max-oRHS)*NewRange/OldRange)


  } else {
    t_max <- l_max
    t_min <- l_min
  }


  transFormula <- function(r_series){
    m <- (r_series-r_min)/(r_max-r_min)
    m*(t_max-t_min)+t_min
  }

  re_transFormula <- function(r_series){
    m <- (r_series-t_min)/(t_max-t_min)
    m*(r_max-r_min)+r_min
  }


  if(is.null(manual_lhs_axis)){

    if(same_origo == TRUE){
      left_axis <- pretty(c(lhs_Limits[1], lhs_Limits[2]))
    }else {
      left_axis <- scales::pretty_breaks(n = 7)
    }

    left_expand <-  expand_arg <- c(0.05, 0)
    left_limit <- NULL
  } else {
    left_axis <- manual_lhs_axis
    left_limit <- c(min(manual_lhs_axis), max(manual_lhs_axis))
    left_expand <- c(0, 0)
  }

  if(same_origo == TRUE){
    if(symmetric_origo){
      right_axis <- pretty(c(r_min, r_max))
      axes <- symmetric_dual_axes(lhs_axis = left_axis,
                                  rhs_axis = right_axis,
                                  lhs_0 =oLHS,
                                  rhs_0 =oRHS)
      left_axis <- axes[['lhs_axis']]
      right_breaks <- axes[['rhs_axis']]
      l_max <- max(left_axis)
      r_max <- max(right_breaks)

      if(oRHS - r_max != 0 && oLHS - l_max !=0){
        transFormula <- function(r_series){
          m = (oLHS - l_max)/(oRHS - r_max)
          return(m*(r_series - oRHS ) + oLHS)
        }

        re_transFormula <- function(l_series){
          m = (oLHS - l_max)/(oRHS - r_max)
          return((l_series - oLHS )/m + oRHS)
        }
      }else if (oRHS - r_min != 0 && oLHS - l_min !=0){
        transFormula <- function(r_series){
          m = (oLHS - l_min)/(oRHS - r_min)
          return(m*(r_series - oRHS ) + oLHS)
       }

        re_transFormula <- function(l_series){
          m = (oLHS - l_min)/(oRHS - r_min)
          return((l_series - oLHS )/m + oRHS)
        }
      }

    }
    else {
      right_breaks <- re_transFormula(left_axis)
      right_breaks_step <- right_breaks[2] - right_breaks[1]

      if(min(right_breaks) - r_min > right_breaks_step){
        # if this is the case we need to extend the axis below

        r_breaks_diff <- min(right_breaks) - r_min
        # this is the number of breaks below we need to extend to
        lower_breaks_pad <- floor(r_breaks_diff/right_breaks_step)

        right_breaks_seq <- seq(right_breaks[1] - right_breaks_step,
                                by = -right_breaks_step,
                                length.out = lower_breaks_pad)
        right_breaks <- c(rev(right_breaks_seq), right_breaks)

        #
        left_axis_step <- left_axis[2] - left_axis[1]
        left_axis_seq <- seq(left_axis[1]-left_axis_step,
                             by =  -left_axis_step,
                             length.out = lower_breaks_pad)
        left_axis <- c(rev(left_axis_seq), left_axis)
      }
    }
      if(is.null(decimals_rhs)){
      decimals_rhs <- 3
    }

  }
  else {
    right_breaks <- scales::pretty_breaks(n = 7)
  }


  p <- ggplot2::ggplot()+
    ggplot2::aes(x=x)


  # Left hand side variables
  if(!is.null(lhs_column)){
    p <- p+ggplot2::geom_col(
      data = dplyr::filter(dLHS, (var %in% lhs_column)),
      ggplot2::aes(y=y, fill = var )
    )
  }

  line_series <- c(rhs_line, lhs_line)
  point_series <- c(rhs_point, lhs_point)






  if(!is.null(lhs_line)){
    if(!is.null(line_series)&!is.null(point_series)){
      p <- p+ggplot2::geom_line(
        data = dplyr::filter(dLHS, (var %in% lhs_line)),
        ggplot2::aes(x=x, y=y, col= var, linetype=var, shape=var), size=1.05
      )
    }

    if(!is.null(line_series)&is.null(point_series)){
      p <- p+ggplot2::geom_line(
        data = dplyr::filter(dLHS, (var %in% lhs_line)),
        ggplot2::aes(x=x, y=y, col= var, linetype=var), size=1.05
      )
    }

  }

  if(!is.null(lhs_point)){
    if(is.null(line_series)&!is.null(point_series)){
      p <- p+ggplot2::geom_point(
        data = dplyr::filter(dLHS, (var %in% lhs_point)),
        ggplot2::aes(x=x, y=y, col = var, shape=var), size =2
      )
    }

    if(!is.null(line_series)&!is.null(point_series)){
      p <- p+ggplot2::geom_point(
        data = dplyr::filter(dLHS, (var %in% lhs_point)),
        ggplot2::aes(x=x, y=y, col = var, linetype=var, shape=var), size =2
      )
    }

  }





  if(!is.null(rhs_line)){
    if(!is.null(line_series)&!is.null(point_series)){
      p <- p+ggplot2::geom_line(
        data = dplyr::filter(dRHS, (var %in% paste(rhs_line, indicate_rhs_by))),
        ggplot2::aes(x=x, y=transFormula(y), col= var, linetype=var, shape=var), size=1.05
      )
    }

    if(!is.null(line_series)&is.null(point_series)){
      p <- p+ggplot2::geom_line(
        data = dplyr::filter(dRHS, (var %in% paste(rhs_line, indicate_rhs_by))),
        ggplot2::aes(x=x, y=transFormula(y), col= var, linetype=var), size=1.05
      )
    }

  }

  if(!is.null(rhs_point)){

    if(is.null(line_series)&!is.null(point_series)){
      p <- p+ggplot2::geom_point(
        data = dplyr::filter(dRHS, (var %in% paste(rhs_point, indicate_rhs_by))),
        ggplot2::aes(x=x, y=transFormula(y), col = var, shape=var), size =2
      )
    }

    if(!is.null(line_series)&!is.null(point_series)){
      p <- p+ggplot2::geom_point(
        data = dplyr::filter(dRHS, (var %in% paste(rhs_point, indicate_rhs_by))),
        ggplot2::aes(x=x, y=transFormula(y), col = var, linetype=var, shape=var), size =2
      )
    }
  }



  p <- p+ggplot2::scale_y_continuous(
    breaks=left_axis,
    minor_breaks = NULL,
    limits = left_limit,
    expand = left_expand,
    labels = lhs_labs,
    sec.axis = ggplot2::sec_axis(
      trans = re_transFormula,
      breaks=right_breaks,
      labels = rhs_labs
    )
  )

}
