horizontal_x_inputValidation<-function(
  p,
  chartType="lineChart",
  d=NULL,
  x=NULL,
  add_LaneLine=TRUE,
  ggplot2_Axis=FALSE,
  manual_Change=NULL,
  column_space=FALSE,
  rhs_space=NULL,
  var = 'var'
){


  axisType <- dplyr::pull(dplyr::select(d,x))

  if(lubridate::is.Date(axisType) & chartType %in% c('columnChart')){

    ggplot2_Axis=TRUE
  }

  if(ggplot2_Axis==FALSE){
    if(is.null(manual_Change)){


      if(is.character(axisType)){
        p<-horizontal_textAxis(p, chartType=chartType)

      } else if(is.numeric(axisType)){

        p<-horizontal_numericAxis(p,chartType=chartType)


      } else if(lubridate::is.Date(axisType)){
        if(is.null(x)){
        p<-horizontal_dateAxis(p,
                               d,
                               LaneLine=add_LaneLine,
                               chartType=chartType,
                               rhs_var=rhs_space)}
        else{
            if(!is.null(var)){
              var = rlang::as_string(var)
            }

         p <- horizontal_dateAxis(p,
                                  d,
                                  LaneLine=add_LaneLine,
                                  chartType=chartType,
                                  x = rlang::as_string(x),
                                  var = var,
                                  rhs_var=rhs_space)
        }

      }

    }
    if(!is.null(manual_Change)){

      if(column_space==FALSE){
        ax_lim<-c(min(manual_Change),max(manual_Change))
      }

      if(column_space==TRUE){
        ax_lim<-c(min(manual_Change)-1,max(manual_Change)+1)
      }

      p<-p+ggplot2::scale_x_continuous(
        breaks=manual_Change,
        expand = c(0,0),
        minor_breaks = NULL,
        labels = manual_Change,
        limits = ax_lim
      )

    }



  }

  p

}
