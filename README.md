# Disclaimer
This project and its contents should not be reported as representing the views of the European Central Bank (ECB). The views expressed are those of the authors and do not necessarily represent those of the ECB.


# Introduction

This package provides visualisations using the style of the European Central Bank for [R](https://www.r-project.org/). 

The package is provided as-is and maintenance is on a best-effort basis. 

# Author's note

_If we endeavor to develop a charting instead of a graphing program, we will accomplish two things. First, we inevitably will offer fewer charts than people want. Second, our package will have no deep structure. Our computer program will be unnecessarily complex, because we will fail to reuse objects or routines that function similarly in different charts. And we will have no way to add new charts to our system without generating complex new code. Elegant design requires us to think about a theory of graphics, not charts._
- Leland Wilkinson The Grammar of Graphics. Statistics and Computing. Springer, New York, NY. https://doi.org/10.1007/0-387-28695-0_1

This library is most certainly a charting program. The aim in constructing it was to give users who were accustomed to using spreadsheet based tools a tool to build charts using R that would look and feel like ECB charts. We do this by pre-constraining many visual aspects, such as colour palette and fontsize, and wrapping the chain of ggplot2 calls into single function calls. We wanted the tool to provide an easy introduction to users to work with graphics in R, so that users who were new to R didn't have to learn both the logic of R and ggplot2 at the same time. On the surface, we think this library helps to flatten the learning curve and back-load some of the complexity of graphing with ggplot2.

We would like to give special thanks to Virginia Nicodemi and Lara Vivian for their contributions to the shaping of this library, as well as to the many colleagues that have made suggestions for improvements and offered other feedback. 

# License

The code is licensed under the European Union Public License 1.2 ([EUPL-1.2](https://spdx.org/licenses/EUPL-1.2) or in [this repository](EUPL-1.2-EN.txt)).


# Contact
If you have general questions about this package then create an issue to this project (e.g. in [Gitlab](https://docs.gitlab.com/ee/user/project/issues/create_issues.html)) and/or provide an improvement as merge request/pull requests (e.g. in [Gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)). See also our [contributor covenant code of conduct](CONTRIBUTING.md).

You can write also to oss_econ_ecbgraphs@ecb.europa.eu and we will answer on a best-effort basis.
