% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/ECB_columnChart.R
\name{ECB_columnChart}
\alias{ECB_columnChart}
\title{Make a column chart consistent with ECB formatting guidelines}
\usage{
ECB_columnChart(
  data,
  x = NULL,
  y = NULL,
  aesthetic_variable = NULL,
  panel_variable = NULL,
  manual_y_axis_lhs = NULL,
  manual_y_axis_lhs_decimals = NULL,
  manual_colours = NULL,
  manual_legend_order = NULL,
  manual_legend_nrow = NULL,
  chart_type_clustured_column = TRUE,
  chart_type_bars = FALSE,
  chart_type_share = FALSE,
  add_shade = NULL,
  format_x_title = NULL,
  format_y_title = NULL,
  format_rotate_labels = FALSE,
  point_series = NULL,
  point_series_size = 2,
  manual_panel_scale = "fixed",
  manual_panel_nrow = NULL,
  y_axis_duplicate = FALSE,
  y_deviate_from = NULL,
  add_vertical_line = NULL,
  add_horizontal_line = NULL,
  manual_x_minor_grid = TRUE,
  keep_default_ggplot2_horizontalAxis = FALSE,
  order_by = NULL
)
}
\arguments{
\item{data}{A data set (preferably long format and retrieved  from internal data sources).}

\item{x}{The column name of the variable for the x-axis (as string).
By default \code{x = "OBS_DATE"}.}

\item{y}{The column name of the variable for the y-axis (as string).
By default \code{y = "OBS_VALUE"}.}

\item{aesthetic_variable}{The column name of the categorical variable (as string) intended for the aesthetics of the lines.
Note that the unique values in the \code{aesthetic_variable} will be displayed in the legend.
By default \code{aesthetic_variable = "SERIES_NAME"}.
In case a single-line chart is desired,  set \code{aesthetic_variable = NULL}.}

\item{panel_variable}{The column name of the variable (as string)
intended to faceting groups on the rows or columns dimension.
Put differently,  including a \code{panel_variable} yields multi-panel plots.}

\item{manual_y_axis_lhs}{A numeric vector to control the values of the left y-axis.
Note that the limits of the y-axis are set to the max and min value of \code{manual_y_axis_lhs},  which might
(unintentionally) remove outliers from the graph. As such,  \code{manual_y_axis_lhs = NULL} is advised for all
automated graphs.}

\item{manual_y_axis_lhs_decimals}{A numeric value to control the number of decimals on the left y-axis.}

\item{manual_colours}{A list of all unique values in the \code{aesthetic_variable} and  the respective colour.
Particularly useful to combine with \code{ECB_col}.}

\item{manual_legend_order}{A vector of all unique values in the \code{aesthetic_variable} ordered as intended to be
displayed in the legend. By default legends are ordered alphabetically.
Note,  \code{manual_legend_order! = NULL} ignores the country specific colour scheme.}

\item{manual_legend_nrow}{A numeric value representing the number of rows to distribute the legend on.
By default,  each series get an individual legend row.
Please ensure that the legend fits the desired output format.}

\item{chart_type_clustured_column}{A boolean that if \code{TRUE} presents the columns as clustered bars.
If \code{chart_type_clustured_column = FALSE} columns are presented as stacked bars.}

\item{chart_type_bars}{A boolean that if \code{TRUE} flips the cartesian coordinates so that
horizontal becomes vertical,  and vertical,  horizontal
(i.e. same as bar chart in Excel).
This is primarily useful for converting geoms and statistics which display y conditional on x,  to x conditional on y.}

\item{chart_type_share}{A boolean that if \code{TRUE} will display a stacked column chart
where each column represents its components share in 100\% of the value, where 100\% is the
sum of the columns}

\item{add_shade}{A shade object created with \code{add_shade}, can be
a horizontal, or vertical shaded area, or a vector of shade_objects.}

\item{format_x_title}{Title of the x-axis (as string).
Note that this should also be added in the \code{save_ECBgraph}.}

\item{format_y_title}{Title of the y-axis (as string).
Note that this should also be added in the \code{save_ECBgraph}.}

\item{format_rotate_labels}{A boolean that if \code{TRUE} rotates the labels on the horizontal axis 90 degrees.
Note that this should also be added in the \code{save_ECBgraph}.}

\item{point_series}{The series (as string) intended to be displayed on the right axis.
The \code{point_series} must correspond to a value in the \code{aesthetic_variable}.
By default,  \code{point_series = NULL},  which yields a regular stacked column chart.
Note that if \code{line_series == point_series},  then the aggregated series is represented
as both points and line.}

\item{point_series_size}{integer setting the size of points of \code{point_series} in mm, by default 2.}

\item{manual_panel_scale}{String to control the scales of the panels.
Could be any of the following: \itemize{
\item "fixed" - (default) same y- and x-axis for each panel
\item "free" - individual y- and x-axis for each panel
\item free in one dimension: "free_x" or"free_y"
}}

\item{manual_panel_nrow}{the number of rows the panel layout should have}

\item{y_axis_duplicate}{whether to duplicate the y-axis on the right hand side}

\item{y_deviate_from}{A numeric value as new origin.
Particularly useful for PMIs to indicate the development relative to 50 or
for series that are index to 100 and show the development relative to 100.}

\item{add_vertical_line}{A value(s) of same format as the x-axis
representing the value(s) where vertical line(s) should be added.
Particularly useful to indicate forecast period, e.g. \code{add_vertical_line=as.Date("2050-01-01")}.}

\item{add_horizontal_line}{A numeric value(s) intended to be displayed as a horizontal line(s).
Particularly useful to plot PMI's and set \code{add_horizontal_line=50}
or y-o-y percentage changes and set \code{add_horizontal_line=0}.}

\item{manual_x_minor_grid}{the spacing for minor grid lines on the x-axis}

\item{keep_default_ggplot2_horizontalAxis}{whether to use the ggplot2 axis or the
horizontal axis determined by ECBgrpahs}

\item{order_by}{This argument is used to re-order the bars. At the moment,  if left NULL the default ordering will apply.
If instead the argument \code{order_by = 'Y'} is used,  the bars will be ordered by the value of the Y-axis  variable in increasing order.
If the argument \code{order_by = '-Y'} is used,  the bars will be ordered by the value of the Y-axis  variable in increasing order.}
}
\description{
ECB_columnChart is a function developed to produce column and bar charts
for economic data frequently used within the ECB.
The function is limited to having the x-axis as text or numeric (i.e. not as date).
}
\examples{
 set.seed(10)
data1 <- as.data.frame(list(a = rep(seq.Date(as.Date('2018-07-01'),
 by = '1 month', length.out = 12),2),
                            b = runif(24, 1, 10),
                            c = c(rep('A',12), rep('B', 12))))

data2 <-  as.data.frame(list(a = seq.Date(as.Date('2018-07-01'),
 by = '1 month', length.out = 12),
                             A = runif(12, 1, 10),
                             B = runif(12, 4, 9),
                             sd = runif(12, .25, .75),
                             sn = 'NAME'))

# Simple single varialbe bar
example1 <- ECBgraphs::ECB_columnChart(data2,
                                       x = 'a',
                                       y = 'A')
example1

example2 <- ECBgraphs::ECB_columnChart(data2,
                                       x = a,
                                       y = A)
example2

# two column chart
example3 <- ECBgraphs::ECB_columnChart(data1,
                                       x = 'a',
                                       y = 'b',
                                       aesthetic_variable = 'c')
example3

example4 <- ECBgraphs::ECB_columnChart(data1,
                                       x = a,
                                       y = b,
                                       aesthetic_variable = c)
example4

# side by side column charts
example5 <- ECBgraphs::ECB_columnChart(data1,
                                       x = 'a',
                                       y = 'b',
                                       aesthetic_variable = 'c',
                                       panel_variable =  'c')
example5

example6 <- ECBgraphs::ECB_columnChart(data1,
                                       x = 'a',
                                       y = 'b',
                                       aesthetic_variable = 'c',
                                       panel_variable =  c)
example6

# A column chart with points
example7 <- ECBgraphs::ECB_columnChart(data1,
                                       x = 'a',
                                       y = 'b',
                                       aesthetic_variable = 'c',
                                       point_series = 'B')
example7

# A bit of a silly example, bars and points on separate panels
example8 <- ECBgraphs::ECB_columnChart(data1,
                                       x = 'a',
                                       y = 'b',
                                       aesthetic_variable = 'c',
                                       panel_variable =  'c',
                                       point_series = 'B')
example8

example9 <- ECBgraphs::ECB_columnChart(data1, x = 'a',
                                       y = 'b',
                                       aesthetic_variable = 'c',
                                       manual_legend_order = c('B', 'A'))
example9

# A bar chart where bars increase left to right
example10 <- ECBgraphs::ECB_columnChart(data2,
                                        x = 'a',
                                        y = 'A', order_by = 'Y')
example10
# Stacked bar graph
example11 <- ECBgraphs::ECB_columnChart(data1,
                                        x = 'a',
                                        y = 'b',
                                        aesthetic_variable = 'c',
                                        chart_type_clustured_column = FALSE)
example11

# Showing how you can add a geom to an existing chart
example12 <- ECBgraphs::ECB_columnChart(data2,
                                        x = 'a',
                                        y = A) +
  ggplot2::geom_point(
    ggplot2::aes(x = a, y = B) , color = ECB_col[2])


example12
}
\author{
European Central Bank
}
